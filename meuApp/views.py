from django.shortcuts import render, redirect, get_object_or_404
from .models import Cliente
from .forms import ClienteForm

# Create your views here.

def clienteList(request):
    clientes = Cliente.objects.all()

    return render(request, 'clienteList.html', {'clientes': clientes})

def clienteCreate(request):
    form = ClienteForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('clienteList')

    return render(request, 'clienteCreate.html', {'form': form})

def clienteUpdate(request, id):
    cliente = get_object_or_404(Cliente, pk=id)
    form = ClienteForm(request.POST or None, instance=cliente)

    if form.is_valid():
        form.save()
        return redirect('clienteList')

    return render(request, 'clienteCreate.html', {'form': form})

def clienteDelete(request, id):
    cliente = get_object_or_404(Cliente, pk=id)

    if request.method == 'POST':
        cliente.delete()
        return redirect('clienteList')

    return render(request, 'clienteDelete.html', {'cliente': cliente})